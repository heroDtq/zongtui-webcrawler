package com.zongtui.fourinone.worker;

import com.zongtui.fourinone.file.WareHouse;

/**
 * 工人接口.
 */
public interface Workman
{
    /**
     * 接收【手工仓库】
     * @param inhouse 手工仓库对象.
     * @return
     */
	public boolean receive(WareHouse inhouse);
	public String getHost();
	public int getPort();
}