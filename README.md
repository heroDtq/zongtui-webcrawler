#zongtui-webcrawler

众推，基于机器学习和深度学习的开源项目，主要处理网络爬虫、文本挖掘等相关的内容！

1）实现基于hadoop思维的分布式网络爬虫，在完善和深入。
2）实现各种包括去重分析、关键字提取、情感分析、文本分类处理等。

核心设计思路是可以接入任何一种爬虫，并且可以针对此种爬虫的问题进行扩展。

<img src="http://images.cnitblog.com/blog/133059/201504/301319592714923.jpg"></img>

欢迎加入讨论：众推 194338168

rule是规则解析
core是核心调度
plugin是插件
sourceer是数据源，也就是爬虫
store是存储

以下为爬虫列表：
http://www.oschina.net/search?scope=project&q=%E7%BD%91%E7%BB%9C%E7%88%AC%E8%99%AB   

项目中所用的编译版本为JDK1.7。

大数据博客整理：
https://github.com/jxqlovejava/PopularBlogSites/blob/master/README.md
